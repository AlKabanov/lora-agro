

void radioTest(uint8_t *data, uint8_t dataLen,uint8_t power);

void nextTx(uint8_t *data, uint8_t dataLen, bool crc16On);

void radioSleep(void);

typedef enum 
{
  band0 = (uint32_t)((864100UL << 11)/125),  //864.1MHz
  band1 = (uint32_t)((864300UL << 11)/125),  //864.3MHz
  band2 = (uint32_t)((864500UL << 11)/125),
  band3 = (uint32_t)((864640UL << 11)/125),
  band4 = (uint32_t)((864780UL << 11)/125),
  band5 = (uint32_t)((868780UL << 11)/125),
  band6 = (uint32_t)((868950UL << 11)/125),
  band7 = (uint32_t)((869120UL << 11)/125),
  bandLoraCom = (uint32_t)(864600UL << 11)/125,
  bandFSK = (uint32_t)(864800UL << 11)/125,
} band_t;