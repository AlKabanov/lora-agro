
#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include "gpio.h"

#define LED_PORT      gpioPortC           
#define LED_BIT       15				
#define LED_MASK      (1 << LED_BIT)

#define PIN_SPI_CS                13			//CSN = PE13
#define PORT_SPI_CS               gpioPortE

#define RST_PORT gpioPortB
#define RST_PIN  14	

#define TEMP_PWR_PORT gpioPortB
#define TEMP_PWR_BIT  11
#define TEMP_PWR_EN   GPIO_PinModeSet(TEMP_PWR_PORT, TEMP_PWR_BIT, gpioModePushPull, 0)
#define TEMP_PWR_ON   GPIO_PinOutSet(TEMP_PWR_PORT, TEMP_PWR_BIT)
#define TEMP_PWR_OFF  GPIO_PinOutClear(TEMP_PWR_PORT, TEMP_PWR_BIT)

#define PULS_PWR_PORT gpioPortC
#define PULS_PWR_BIT  0
#define PULS_PWR_EN   GPIO_PinModeSet(PULS_PWR_PORT, PULS_PWR_BIT, gpioModePushPull, 0);\
                      GPIO_DriveModeSet(PULS_PWR_PORT,gpioDriveModeHigh)

#define PULS_PWR_ON   GPIO_PinOutSet(PULS_PWR_PORT, PULS_PWR_BIT)
#define PULS_PWR_OFF  GPIO_PinOutClear(PULS_PWR_PORT, PULS_PWR_BIT)

#define LED_EN  GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModePushPull, 0)
#define LED_DIS GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModeDisabled, 0)
#define LED_ON  GPIO_PinOutSet(LED_PORT, LED_BIT)
#define LED_OFF GPIO_PinOutClear(LED_PORT, LED_BIT)
#define LED_TOGGLE GPIO_PinOutToggle(LED_PORT, LED_BIT)

#define LDO_PORT      gpioPortF           
#define LDO_BIT       2		
#define LDO_EN  GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModePushPull, 0)
#define LDO_DIS GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModeDisabled, 0)
#define LDO_ON  GPIO_PinOutSet(LDO_PORT, LDO_BIT)
#define LDO_OFF GPIO_PinOutClear(LDO_PORT, LDO_BIT)





/**************************************************************************//**
 * @brief Setup GPIO interrupt to set the time
 *****************************************************************************/
void gpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  GPIO_PinModeSet(PORT_SPI_CS, PIN_SPI_CS, gpioModePushPull, 1);
  
 //  Configure PB13 as input 
  GPIO_PinModeSet(gpioPortB, 13,  gpioModeInputPullFilter, 1); //button
/*
  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 10, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);

  // Configure PB9 as input 
  GPIO_PinModeSet(gpioPortB, 9, gpioModeInput, 0);
*/
  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 13, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);

  LED_EN;
  LDO_EN;
  TEMP_PWR_EN;
  PULS_PWR_EN;
}

void gpioSetLED(void)
{
  LED_ON;
}

void gpioClearLED(void)
{
  LED_OFF;
}

void gpioToggleLED(void)
{
  LED_TOGGLE;
}
void gpioLDOOn(void)
{
  LDO_ON;
}

void gpioLDOOff(void)
{
  LDO_OFF;
}

void gpioTEMPPWRON(void)
{
  TEMP_PWR_ON;
}

void gpioTEMPPWROFF(void)
{
  TEMP_PWR_OFF;
}
void gpioPULSEPWRON(void)
{
  PULS_PWR_ON;
}

void gpioPULSEPWROFF(void)
{
  PULS_PWR_OFF;
}

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val)
{
  if(val == 0 || val == 1)
  { // drive pin
       GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModePushPull, 0);
       if (val)GPIO_PinOutSet(RST_PORT, RST_PIN);
       else    GPIO_PinOutClear(RST_PORT, RST_PIN);
  } else 
    { // keep pin floating
        GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModeDisabled, 1);
    }
}

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val)
{
	//not used 
}

// set radio NSS pin to given value
void gpioNSS (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Enable (Active Low)
		
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB9)
 *        Sets the hours
 *****************************************************************************/

bool sleepFlag = false;

void GPIO_ODD_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << 13); 
  sleepFlag = true;
}

bool gpioGetSleepFlag(void)
{
  bool temp = sleepFlag;
  sleepFlag = false;
  return temp;
}



/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB10)
 *        Sets the minutes
 *****************************************************************************/
/*
void GPIO_EVEN_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << 10);

  // Increase minutes 
  minutes = (minutes + 1) % 60;
}
*/