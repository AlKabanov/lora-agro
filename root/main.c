/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 ******************************************************************************
 * @section License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"

#define MEASUR_PERIOD 40
#define SEND_PERIOD 4000
#define POWER 2

void sleepMode(void)
{
  gpioPULSEPWROFF();
  gpioClearLED();
  radioSleep();
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  NVIC_DisableIRQ(GPIO_ODD_IRQn);
  EMU_EnterEM2(true);
}

uint8_t data[SEND_PERIOD/MEASUR_PERIOD + 3];  //50 - data 1 - period 2 - crc
//uint8_t data[53];
uint8_t dataLen = sizeof(data);	


/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  for(int i = 0; i < dataLen;i++)data[i] = i;
  gpioLDOOn();
  radioTest(data, dataLen,POWER);
  gpioPULSEPWRON();
  rtcWait(2000);
  while (1)
  {
    for(int i = 0; i < dataLen - 3; i++)
    {
     
      data[i] = adcPulseRead();
      uartSend(data[i]);
      rtcWait(MEASUR_PERIOD);     
      if(gpioGetSleepFlag()) //go to sleep
      {
        sleepMode();
      }
    }
    data[dataLen - 3] = MEASUR_PERIOD;
    gpioToggleLED();
    nextTx(data,dataLen,true);
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();


  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();  

  /* Setup GPIO interrupt to set the time */
  gpioSetup();
  
  spiSetup();
  
  adcInit();
  
  uartInit(115200);
  
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  
  
  
  /* Main function loop */
  clockLoop();

  return 0;
}
